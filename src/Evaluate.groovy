import java.util.regex.Matcher
import java.util.regex.Pattern

public enum Evaluate {

	NOT_EQUAL,
	EQUAL,
	EQUAL_IGNORE_CASE,
	CONTAINS,
	DOES_NOT_CONTAINS,
	LESS_THAN,
	LESS_THAN_OR_EQUAL_TO,
	GREATER_THAN,
	GREATER_THAN_OR_EQUAL_TO,
	IS_NULL,
	NOT_NULL,
	IS_IN,
	NOT_IN,
	LIKE,
	NOT_LIKE,
	ILIKE,
	NOT_ILIKE,
	AND,
	OR,
	EXIST,
	BETWEEN,
	NOT_BETWEEN

	def evaluate(val1, val2) {
		switch(this) {
			case NOT_EQUAL:
			case IS_NULL:
			case NOT_IN:
				return val1 != val2
			case EQUAL:
			case EXIST:
			case NOT_NULL:
			case IS_IN:
				return val1 == val2
			case EQUAL_IGNORE_CASE:
				return String.valueOf(val1).equalsIgnoreCase(val2)
			case CONTAINS:
				return String.valueOf(val1).contains(val2)
			case DOES_NOT_CONTAINS:
				return !String.valueOf(val1).contains(val2)
			case LESS_THAN:
				return val1 instanceof Date ? val1 < val2 : Double.valueOf(val1) < Double.valueOf(val2)
			case LESS_THAN_OR_EQUAL_TO:
				return val1 instanceof Date ? val1 <= val2 : Double.valueOf(val1) <= Double.valueOf(val2)
			case GREATER_THAN:
				return val1 instanceof Date ? val1 > val2 : Double.valueOf(val1) > Double.valueOf(val2)
			case GREATER_THAN_OR_EQUAL_TO:
				return val1 instanceof Date ? val1 >= val2 : Double.valueOf(val1) >= Double.valueOf(val2)
			case LIKE:
			case ILIKE:
			case NOT_LIKE:
			case NOT_ILIKE:
				Pattern pattern = null
				if (this.name().contains("ILIKE")) {
					pattern = Pattern.compile(val2.replaceAll("\\?", ".").replaceAll("%", ".*"), Pattern.CASE_INSENSITIVE)
				} else {
					pattern = Pattern.compile(val2.replaceAll("\\?", ".").replaceAll("%", ".*"))
				}
				Matcher matcher = pattern.matcher(val1)
				return this.name().contains("NOT") ? !matcher.matches() : matcher.matches()
			case AND:
				return val1 && val2
			case OR:
				return val1 || val2
				case BETWEEN:
				return val1 instanceof Date ? val1 >= val2 : Double.valueOf(val1) >= Double.valueOf(val2)
			default:
				return null
		}
	}
	
	def evaluate(val1, val2, val3) {
		switch (this) {
			case BETWEEN:
				return val1 instanceof Date ? val3 >= val1 && val3 <= val2 : Double.valueOf(val3) >= Double.valueOf(val1) && Double.valueOf(val3) <= Double.valueOf(val2)
			case NOT_BETWEEN:
				return val1 instanceof Date ? val3 < val1 || val3 > val2 : Double.valueOf(val3) < Double.valueOf(val1) || Double.valueOf(val3) > Double.valueOf(val2)
			default:
			return null
		}
	}
}


