class Node extends Holder {

	private def node

	public def setNode(node) {
		this.node = node
	}

	public def getNode() {
		return this.node
	}
	
	public def setNodeValue(nodeToGet = getNode(), value) {
		getHolder().setNodeValue("//$nodeToGet", value)
	}

	public def getNodeCount(nodeToGet = getNode()) {
		return getNodeValues(nodeToGet).size()
	}

	public def getNodeValue(nodeToGet = getNode()) {
		return getHolder().getNodeValue("//$nodeToGet")
	}

	public def getNodeValues(nodeToGet = getNode()) {
		return getHolder().getNodeValues("//$nodeToGet")
	}

	public def getDomNode(nodeToGet = getNode()) {
		return getHolder().getDomNode("//$nodeToGet")
	}

	public def getDomNodes(nodeToGet = getNode()) {
		return getHolder().getDomNodes("//$nodeToGet")
	}

	public def getNodeAttributeValue(nodeToGet = getNode(), attr) {
		return getDomNode(nodeToGet).getAttributes().getNamedItem(attr).value
	}

	public def getNodeValueList(nodeToGet = getNode(), val="value") {
		def valueList = []
		for (nv in getNodeValues("$nodeToGet/$val")) {
			valueList.add(nv)
		}
		return valueList
	}

	public def getNodeChildren(nodeToGet = getNode(), excludeList = [""]) {
		def list = []
		getDomNode(nodeToGet).getChildNodes().each {
			def localName = it.getLocalName()
			
			if (localName != null) {
				boolean excluded = false
				for (excl in excludeList) {
					if (localName.equalsIgnoreCase(excl)) {
						excluded = true
						break
					}
				}
				if (!excluded)
					list.add(localName)
			}
		}
		return list
	}
	
	public def getNodeChildrenMap(nodeToGet = getNode(), excludeList = [""]) {
		def map = [:]
		getDomNode(nodeToGet).getChildNodes().each {
			def localName = it.getLocalName()
			
			if (localName != null) {
				boolean excluded = false
				for (excl in excludeList) {
					if (localName.equalsIgnoreCase(excl)) {
						excluded = true
						break
					}
				}
				if (!excluded) {
					map.put(localName, it.getFirstChild() == null ? "" : it.getFirstChild().getNodeValue())
				}
			}
		}
		return map
	}
}
