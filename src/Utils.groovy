import java.util.HashMap

class Utils {

	private TestRunner tr = null
	private String defaultPid = null
	private int jobStatusQueryLimit
	private Users users = null;

	public Utils(TestRunner tr) {
		this.tr = tr
		this.defaultPid = tr.getProjectProperty("ReadTestPid")
		try {
		this.jobStatusQueryLimit = Integer.valueOf(tr.getProjectProperty("jobStatusQueryLimit"))
		} catch (Exception) {
			throw new TestRunnerException("jobStatusQueryLimit is not set in project.  Please add the custom property and set it to an integer value.")
		}
		this.users = new Users(tr)
	}

	public TestRunner getTestRunner() {
		return this.tr
	}

	public String getDefaultPid() {
		return this.defaultPid
	}
	
	public Users Users() {
		return this.users
	}

	public HashMap<String,Holder> getJobStatus(String jobId, int count = 0, HashMap<String,String> provided = [:]) {
		def default_map = [
		"JobId":jobId
		]
		provided = fillInGaps(provided, default_map)
		
		HashMap<String,Holder> res = null
		String status = null

		if (jobId.contains(".")) {
			res = getTestRunner().runStep("Base", "DVL_000", "GetJobRequest", provided)
			status = getTestRunner().getLastResponse().getNodeValue("StatusSummary")
			if (!status.equals("Successful") && !status.equals("Failed")) {
				getTestRunner().getLog().info "$jobId has a status of $status checking again.."
				if (count >= jobStatusQueryLimit) {
					return res
				}
				sleep(500)
				getJobStatus(jobId, count + 1, provided)
			} else if (status.equals("Failed")) {
				throw new TestRunnerException("getJobStatus had a status of Failed.  Stopping.")
			}
		} else {
			res = getTestRunner().runStep("Base", "DVL_000", "GetJobStatusRequest", provided)
			status = getTestRunner().getLastResponse().getNodeValue("JobStatus")
			if (!status.equals("COMPLETE") && !status.equals("FAILED")) {
				getTestRunner().getLog().info "$jobId has a status of $status checking again.."
				if (count >= jobStatusQueryLimit) {
					return res
				}
				sleep(500)
				getJobStatus(jobId, count + 1, provided)
			} else if (status.equals("FAILED")) {
				throw new TestRunnerException("getJobStatus had a status of FAILED.  Stopping.")
			}
		}
		return res
	}

	public HashMap<String,String> fillInGaps(HashMap<String,String> provided, HashMap<String,String> default_map) {
		default_map.each {
			if (provided.get(it.key) == null) {
				provided.put(it.key, it.value)
			}
		}
		return provided
	}
}
