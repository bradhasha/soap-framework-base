class Join {
	
	private JoinCondition join1
	private JoinCondition join2
	private Evaluate eval
	
	public Join(JoinCondition join1, JoinCondition join2, Evaluate eval) {
		this.join1 = join1
		this.join2 = join2
		this.eval = eval
	}
	
	public List<JoinCondition> getJoinConditions() {
		return Arrays.asList(this.join1, this.join2)
	}
	
	public Evaluate getCondition() {
		return this.eval
	}
}