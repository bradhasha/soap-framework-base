

class Assertion extends Node {

	public def assertCount(nodeToGet = getNode(), expectedCount = 0, Evaluate eval = Evaluate.GREATER_THAN, boolean doAssert = true) {
		def count = getNodeValues(nodeToGet).size()
		boolean bool = eval.evaluate(Double.valueOf(count), Double.valueOf(expectedCount))
		if (doAssert) {
			assert bool, "$nodeToGet count $count did not pass evaluation [$eval] to $expectedCount"
		} else {
			return bool
		}
	}

	public def assertLength(nodeToGet = getNode(), expectedLength, Evaluate eval, boolean doAssert = true) {
		boolean bool = false
		for (nv in getNodeValues(nodeToGet)) {
			bool = eval.evaluate(nv.length(), expectedLength)

			if (doAssert)
				assert bool, "$nv length "+ nv.length() + " did not pass evaluation [$eval] to $expectedLength"
			else if (!bool)
				break
		}
		return bool
	}

	public def assertAll(nodeToGet = getNode(), val, Evaluate eval, boolean doAssert = true) {
		boolean bool = false
		if (!eval.name().equals("IS_NULL") && !eval.name().equals("NOT_NULL")) {

			if (getNodeValues(nodeToGet).size() == 0)
				assert false, "$nodeToGet not found"
			// Loop through each node's value
			if (eval.name().equals("EXIST") || eval.name().equals("IS_IN")) {

				if (!(val instanceof List)) {
					def list = [val]
					val = list
				}

				if (val instanceof List) {
					for (v in val) {
						bool = false
						for (nv in getNodeValues(nodeToGet)) {
							if (eval.evaluate(nv, v)) {
								bool = true
								break
							}
						}

						if (doAssert) {
							assert bool, "$val did not pass evaluation [$eval] in $nodeToGet"
							if (bool) {
								break
							}
						} else if (!bool) {
							break
						}
					}
				}
			} else {
				for (nv in getNodeValues(nodeToGet)) {

					// If val is a list check to see if one of the values in the list meets the eval condition
					if (val instanceof List) {

						for (v in val) {
							bool = eval.evaluate(nv, v)

							// If one of the values in the list meets the condition, then break out of the loop
							// and move to the next value to check, or quit if the condition was not met after checking
							// all of the values
							if (!bool) {
								break
							}
						}
					} else {
						bool = eval.evaluate(nv, val)
					}

					// If we are asserting this will perform the assertion
					if (doAssert) {
						assert bool, "$nv did not pass evaluation [$eval] to $val in $nodeToGet"
					} else if (!bool) {
						break
					}
				}
			}
		} else {

			// Only used for IS NULL and NOT NULL
			// Loop through each dom node (parent) and check the children to see if the
			// expected node exists for each parent
			for (domNode in getDomNodes(nodeToGet)) {
				bool = false
				for (childNode in domNode.getChildNodes()) {
					if (childNode.nodeName == val) {
						bool = true
						break
					}
				}

				// If we find the attribute when we do not expect it, or we do not find the attribute when we expect it
				// for a parent node, then we will break out of the loop and fail.  If everything passes bool should be set to true
				// on the final loop.
				if ((bool && eval.name().equals("IS_NULL")) || (!bool && eval.name().equals("NOT_NULL"))) {
					break
				}
			}

			// If we are asserting this will perform the assertion
			if (doAssert) {
				if (eval.name().equals("NOT_NULL")) {
					assert bool, "$val attribute did not pass evaluation [$eval] in $nodeToGet"
				} else {
					assert !bool, "$val attribute did not pass evaluation [$eval] in $nodeToGet"
				}
			}
		}
		return eval.name().equals("IS_NULL") ? !bool : bool
	}

	private def getMatches(nodeToGet = getNode(), val, Evaluate eval, HashMap<String,String> map) {
		def nvs = getNodeValues(nodeToGet)
		// Loop through each node's value
		for (def i=0; i < nvs.size(); i++) {
			// If val is a list check to see if one of the values in the list meets the eval condition
			if (val instanceof List) {

				for (v in val) {
					if (eval.evaluate(nvs[i], v)) {
						map.put(""+i, nvs[i])
						break
					}
				}
			} else {
				if (eval.evaluate(nvs[i], val)) {
					map.put(""+i, nvs[i])
				}
			}
		}
		return map
	}

	public def assertDates(nodeToGet = getNode(), val, Evaluate eval, df = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", boolean doAssert = true) {
		boolean bool = false
		for (next in getNodeValues(nodeToGet)) {
			def baseDate = getDate(val);
			def nextDate = getDate(next);
			
			bool = eval.evaluate(nextDate, baseDate)
			if (doAssert)
				assert bool, "$nextDate did not pass evaluation [$eval] to the base date $baseDate"
			else if (!bool)
				break
		}
		return bool
	}
	
	private def getDate(val) {
		def parsedDate = null	
		try {
			parsedDate = Date.parse("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", val)
		} catch (Exception e) {
			
			try {
				parsedDate = Date.parse("yyyy-MM-dd'T'HH:mm:ss'Z'", val)
			} catch (Exception e1) {
				
				try {
					parsedDate = Date.parse("yyyy-MM-dd'T'HH:mm:ss.SSSS'Z'", val)
				} catch (Exception e2) {
					
					try {
						parsedDate = Date.parse("yyyy-MM-dd'T'HH:mm:ss.SSSSS'Z'", val)
					} catch (Exception e3) {
						
						try {
							parsedDate = Date.parse("yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'", val)
						} catch (Exception e4) {
							
						}
					}
				}
			}
		}
		return parsedDate
	}

	public def assertBNB(nodeToGet = getNode(), lower, upper, Evaluate eval, boolean isDate = false, df = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", boolean doAssert = true) {
		boolean bool = false
		if (isDate) {
			for (next in getNodeValues(nodeToGet)) {
				def lowerDate = getDate(lower)
				def upperDate = getDate(upper)
				def nextDate = getDate(next)

				bool = eval.evaluate(lowerDate, upperDate, nextDate)
				if (doAssert)
					assert bool, "$nextDate did not pass evaluation [$eval] between $lowerDate and $upperDate"
				else if (!bool)
					break
			}
		} else {
			for (next in getNodeValues(nodeToGet)) {
				bool = eval.evaluate(lower, upper, next)
				if (doAssert)
					assert bool, "$next did not pass evaluation [$eval] between $lower and $upper"
				else if (!bool)
					break
			}
		}
		return bool
	}

	public def assertJoin(totalExpectedResults = getNodeCount(), Join join, boolean doAssert = true) {
		List<JoinCondition> jcs = join.getJoinConditions()

		HashMap<String,String> results = new HashMap<>();
		for (JoinCondition jc : jcs) {
			results = getMatches(jc.getNode(), jc.getValues(), jc.getCondition(), results)
		}

		boolean result = results.size() == totalExpectedResults

		if (doAssert) {
			assert result, jcs.get(0).toString() + "\n"+ jcs.get(1).toString() + "\nJoined With " + join.getCondition()+ " had a result of FALSE, total records: " + results.size()
		} else {
			return result
		}
	}

	public def assertGroup(totalExpectedResults = getNodeCount(), Join join1, Join join2, boolean doAssert = true) {

		List<JoinCondition> join1Conditions = join1.getJoinConditions()
		List<JoinCondition> join2Conditions = join2.getJoinConditions()

		HashMap<String,String> results = new HashMap<>();
		def join1Results,join2Results
		for (JoinCondition jc : join1.getJoinConditions()) {
			results = getMatches(jc.getNode(), jc.getValues(), jc.getCondition(), results)
			join1Results = results
		}

		for (JoinCondition jc : join2.getJoinConditions()) {
			results = getMatches(jc.getNode(), jc.getValues(), jc.getCondition(), results)
			join2Results = results
		}

		boolean result = results.size() == totalExpectedResults

		if (doAssert) {
			assert result, "Group Result=$result \n--BEGIN JOIN 1\n" + join1Conditions.get(0).toString() + "\n" + join1Conditions.get(1).toString()+"\nResults=$join1Results\n--END JOIN 1\n--BEGIN JOIN 2\n" + join2Conditions.get(0).toString() + "\n" + join2Conditions.get(1).toString()+"\nResults=$join2Results\n--END JOIN 2"
		} else {
			return result
		}
	}
}
