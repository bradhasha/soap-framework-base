class VerifyNode {

//	static void main(String[] args) {
//		String output = ""
//		for (int i = 55; i < 78; i++) {
//			output += "DVL_001_0" + i + ","
//		}
//		println output
//	}

	// Main components
	private RequestHolder request = null
	private ResponseHolder response = null

	public VerifyNode(log, messageExchange, context) {
		this(log, null, messageExchange, null, context)
	}

	public VerifyNode(log, messageExchangeRequest, messageExchangeResponse, node, context) {
		request = new RequestHolder()
		request.setLog(log)
		request.setNode(node)
		request.setContext(context)
		request.setMessageExchange(messageExchangeRequest)
		request.initializeHolder()

		response = new ResponseHolder()
		response.setLog(log)
		response.setNode(node)
		response.setContext(context)
		response.setMessageExchange(messageExchangeResponse)
		response.initializeHolder()
	}

	public VerifyNode(log, messageExchange, node, context) {
		this(log, messageExchange, messageExchange, node, context)
	}

	public RequestHolder getRequest() {
		return this.request
	}

	public ResponseHolder getResponse() {
		return this.response
	}
}