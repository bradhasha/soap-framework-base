import com.eviware.soapui.support.GroovyUtils
import com.eviware.soapui.support.XmlHolder

class RequestHolder extends Assertion {

	public RequestHolder() {
	}

	@Override
	public def initializeHolder() {
		// Initialize request holder
		try {
			setHolder(new GroovyUtils(getContext()).getXmlHolder(getMessageExchange()))
		} catch (Exception e) {
			try {
				setHolder(new XmlHolder(getContext().expand(getMessageExchange().modelItem.testStep.getProperty("Request").value)))
			} catch (Exception e1) {
				try {
					setHolder(new XmlHolder(getContext().expand(getMessageExchange().getXml())))
				} catch (Exception e2) {
					setHolder(new XmlHolder(getContext().expand(getMessageExchange())))
				}
			}
		}
	}

	@Override
	public def setMessageExchange(messageExchange) {
		super.setMessageExchange(messageExchange)
		initializeHolder()
	}

}