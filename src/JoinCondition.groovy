class JoinCondition {

	private def node
	private def val
	private Evaluate eval
	private boolean isDates
	private def df = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"

	public JoinCondition() {
	}

	public JoinCondition(node, val, Evaluate eval, isDates = false, df = null) {
		this.node = node
		this.val = val
		this.eval = eval
		this.isDates = isDates

		if (df != null)
			this.df = df
	}

	public def setNode(node) {
		this.node = node
	}

	public def setValues(val) {
		this.val = val
	}

	public def setCondition(Evaluate eval) {
		this.eval = eval
	}

	public def setIsDates(isDates) {
		this.isDates = isDates
	}

	public def setDateFormat(df) {
		this.df = df
	}

	public def getNode() {
		return this.node
	}

	public def getValues() {
		return this.val
	}

	public Evaluate getCondition() {
		return this.eval
	}

	public boolean getIsDates() {
		return this.isDates
	}

	public String getDateFormat() {
		return this.df
	}

	@Override
	public String toString() {
		return "JoinCondition: [node:"+getNode()+", value(s):"+getValues()+", condition:"+getCondition()+", isDates:"+getIsDates() + (getIsDates() ? ", dateFormat:"+getDateFormat() : "") +"]"
	}
}