public class Users {

	private TestRunner tr;
	private String user;

	public Users USER;

	public Users(TestRunner tr) {
		this.USER = new Users(tr, "USER")
	}

	public Users(TestRunner tr, String user) {
		this.tr = tr
		this.user = user
	}
	
	/**
	 * Get name of the user
	 * @return
	 */
	public String getUser() {
		return user.substring(0, user.lastIndexOf("_"));
	}

	/**
	 * Get user uuid and format
	 * @return
	 */
	public String uuid() {
		return getUser() + "###" + tr.getProjectProperty(user + "UUID") + "###" + tr.getProjectProperty("NAMEIDFORMAT_UUID")
	}

	public enum Type {
		UUID
	}
}


