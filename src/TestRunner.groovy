import com.eviware.soapui.impl.support.*
import com.eviware.soapui.impl.wsdl.WsdlInterface
import com.eviware.soapui.impl.wsdl.support.wsa.WsaConfig
import com.eviware.soapui.impl.wsdl.teststeps.WsdlGroovyScriptTestStep
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestRequest
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestRequestStep
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestStep
import com.eviware.soapui.support.XmlHolder

class TestRunner {

	private def testRunner
	private def originatingTestCase
	private def context
	private def log
	private HashMap<String,VerifyNode> executedSteps = new HashMap<>()
	private def testSuite
	private def testCase
	private def lastStepRan
	private def lastStepStatus
	private boolean debug = false

	public def TestRunner(log, testRunner, context) {
		this.log = log
		this.testRunner = testRunner
		this.context = context
		this.testCase = context.testCase
		this.originatingTestCase = context.testCase
		this.testSuite = context.testCase.testSuite
	}

	public void setDebug(boolean bool) {
		this.debug = bool
	}

	public boolean getDebug() {
		return this.debug
	}

	public def getContext() {
		return this.context
	}

	public def getLog() {
		return this.log
	}

	public def getTestRunner() {
		return this.testRunner
	}

	public def setTestCaseToDefault() {
		this.testCase = getContext().testCase
		this.testSuite = getContext().testCase.testSuite
	}

	public def setTestCase(ts, tc, boolean exactMatch = false) {
		this.testSuite = findTestSuite(ts, exactMatch)
		this.testCase = findTestCase(testSuite, tc, exactMatch)
	}

	public def getTestSuite() {
		return this.testSuite
	}

	public def getTestCase() {
		return this.testCase
	}

	public def getTestSteps(excludeList = [], boolean exactMatch = false) {
		def steps = getTestCase().getTestSteps()
		steps.entrySet().removeIf( { next ->
			if (next.getKey().startsWith("tmp")) {
				return true
			}
			if (excludeList instanceof List) {
				for (exclude in excludeList) {
					if ((exactMatch ? next.getKey().equalsIgnoreCase(exclude) : next.getKey().contains(exclude))) {
						return true
					}
				}
				return false
			} else {
				return (exactMatch ? next.getKey().equalsIgnoreCase(excludeList) : next.getKey().contains(excludeList))
			}
		})
		return steps
	}

	public def getTestStep(step, boolean exactMatch = false) {
		return extract(getTestSteps(), step, exactMatch)
	}

	private def extract(listOrMap, searchFor, boolean exactMatch = false) {
		def keys = listOrMap instanceof List ? listOrMap.stream() : listOrMap.keySet().stream()
		def val = keys.filter { next ->
			exactMatch ? next instanceof String ? next.equalsIgnoreCase(searchFor) : next.getName().equalsIgnoreCase(searchFor) : next instanceof String ? next.contains(searchFor) : next.getName().contains(searchFor)
		}.findFirst().get()
		return listOrMap instanceof List ? val : listOrMap.get(val)
	}

	public def findTestCase(testSuite, searchFor, boolean exactMatch = false) {
		return extract(testSuite.getTestCaseList(), searchFor, exactMatch)
	}

	public def findTestSuite(searchFor, boolean exactMatch = false) {
		return extract(getTestCase().testSuite.project.getTestSuiteList(), searchFor, exactMatch)
	}

	public HashMap<String, Holder> runStep(WsdlTestStep tempStep) {

		// Execute test step, if fail then throw exception
		def executeStep = tempStep.run(getTestRunner(), getContext())
		lastStepStatus = executeStep.getStatus().toString()
		if (lastStepStatus != "OK" && lastStepStatus != "UNKNOWN") {
			throw new TestRunnerException("$lastStepRan had a status of $lastStepStatus.  Stopping. \n" + tempStep.getPropertyValue("Response"))
		}

		// Create a new VerifyNode object with the request/response that we can use if needed and add it to map
		VerifyNode vn = new VerifyNode(getLog(), tempStep.getPropertyValue("Request"), tempStep.getPropertyValue("Response"), null, getContext())
		executedSteps.put(lastStepRan, vn)

		HashMap<String,Holder> results = new HashMap();
		results.put("Request", getLastRequest())
		results.put("Response", getLastResponse())
		return results
	}

	public void runStep(WsdlGroovyScriptTestStep tempStep) {
		tempStep.run(getTestRunner(), getContext())
	}

	public HashMap<String, Holder> runStep(String step, boolean exactMatch = false) {
		// Get test step name
		return runStep(step, [:])
	}

	public HashMap<String, Holder> runStep(tc = getTestCase().getName(), ts = getTestSuite().getName(), String step, HashMap<String,String> nodeMap, boolean exactMatch = false) {
		// Set test case so we can find the right step
		setTestCase(ts, tc, exactMatch)

		// Extract user from hashmap if it exists
		String user = nodeMap.get("user")

		// Get test step
		def retrieved = getTestStep(step, exactMatch)
		if (retrieved instanceof WsdlGroovyScriptTestStep) {
			runStep(retrieved)
		} else {
			lastStepRan = getTestSuite().getName() + "_" + getTestCase().getName() + "_" + retrieved.getName() + "_" + System.nanoTime()

			// Copy test step to test case using the step as a temp request
			def tempTc = getContext().testCase
			def tempStep = null

			for (int i = 0; i < 20; i++) {
				try {
					tempStep = retrieved.clone(tempTc, "tmp$lastStepRan")
					break
				} catch (Exception e) {
					try {
						Thread.sleep(new Random().nextInt((5000 - 1000) + 1) + 1000)
						tempStep = retrieved.clone(tempTc, "tmp$lastStepRan")
						break
					} catch (Exception e1) {
						i++
					}
				}
			}

			if (tempStep == null)
				return null

			// Get request holder, and update the node values according to the map
			RequestHolder rh = getDefaultRequest(tempStep)
			nodeMap.each {
				if (!it.key.equals("user"))
					rh.setNodeValue(it.key, it.value)
			}

			// Update the default request
			tempStep.setPropertyValue("Request", rh.getHolder().getXml())

			if (user != null) {
				String[] data = user.split("###")
				getLog().info "Updating request to use user " + data[0] + ": " + data[1] + " in format: " + data[2]
				setTestCaseProperty("NameOfUser", data[0])
				setTestCaseProperty("User", data[1])
				setTestCaseProperty("NameIdHeader", data[2])
				setWSSecurityConfig(tempStep.getTestRequest())
			}

			// Run the temp step
			HashMap<String,Holder> results = runStep(tempStep)
			results.put("Body", getContext().expand('${' + tempStep.getName() + '#Response#' + nodeMap.get("bodyTag") != null ? 
				nodeMap.get("bodyTag") : "soap:Body"))

			// Remove the temp step
			if (!getDebug())
				tempTc.removeTestStep(tempStep)

			// Set test case back to default
			setTestCaseToDefault()
			getLog().info "Finished executing step $lastStepRan"
			return results
		}
		return null;
	}


	public void setWSSecurityConfig(WsdlTestRequest request, String outgoing = "outgoing-automated", String incoming = "incoming") {
		request.setEndpoint("\${#Project#WsSecurityEndpoint}")
		request.setOutgoingWss(outgoing)
		request.setIncomingWss(incoming)
		WsaConfig wsaConfig = request.getWsaConfig()
		wsaConfig.setWsaEnabled(true)
		wsaConfig.setVersion("200408")
		wsaConfig.setAction("")
		wsaConfig.setGenerateMessageId(true)
	}

	public void setDiscardResultsForAllTestCases(boolean val) {
		getTestCase().testSuite.project.getTestSuiteList().each { tsuite ->
			tsuite.getTestCaseList().each { tcase ->
				tcase.getTestStepList().each { tstep ->
					log.info tstep.getClass().toString()
					if (tstep instanceof WsdlTestRequestStep)
						tstep.getTestRequest().setDiscardResponse(val)
				}
			}
		}
	}

	public void deleteAllTempSteps() {
		getTestCase().testSuite.project.getTestSuiteList().each { tsuite ->
			tsuite.getTestCaseList().each { tcase ->
				tcase.getTestSteps().each { tstep ->
					if (tstep.getName().contains("tmp")) {
						tcase.removeTestStep(step)
					}
				}
			}
		}
	}

	public void setAllRequestHeadersForInterface(String inter = "DVLSoap11", String user = "user") {
		def myInterface = (WsdlInterface) testRunner.testCase.testSuite.project.getInterfaceByName(inter)
		for (next in myInterface.getOperationList()) {
			if (next != null && next.getRequestList().size() > 0) {
				next.getRequestList().each { req ->
					def headers = req.getRequestHeaders()
					headers.clear()
					req.setRequestHeaders(headers)
				}
			}
		}
	}

	public void cloneTestCases(List<String> matches) {
		def sourceProject = testRunner.getTestCase().getTestSuite().getProject().getWorkspace().getProjectByName("Source")
		def targetProject = testRunner.getTestCase().getTestSuite().getProject().getWorkspace().getProjectByName("Target")

		sourceProject.getTestSuiteList().each { tsuite ->

			if (tsuite.getName().contains("DVL_0")) {

				tsuite.getTestCaseList().each { tcase ->

					boolean matched = false
					for (next in matches) {
						matched = next.equals("match") ? tcase.getName().startsWith(next) : tcase.getName().contains(next)
						if (matched)
							break
					}

					if (matched) {
						log.info "Copying " + tcase.getName() + " to " + tsuite.getName()

						targetProject.getTestSuiteByName(tsuite.getName()).cloneTestCase(tcase, "copied"+ tcase.getName())
					}
				}
			}
		}
	}

	public void setAllRequestHeadersForTests(String user) {
		getTestCase().testSuite.project.getTestSuiteList().each { tsuite ->

			tsuite.getTestCaseList().each { tcase ->

				tcase.getTestStepList().each { tstep ->

					if (tstep.config.type.equals("request")) {
						def headers = tstep.getTestRequest().requestHeaders
						headers.clear()
						tstep.getTestRequest().requestHeaders = headers
					}
				}
			}
		}
	}

	private RequestHolder getDefaultRequest(step) {
		RequestHolder rh = new RequestHolder()
		rh.setHolder(new XmlHolder(getContext().expand(step.getPropertyValue("Request"))))
		return rh
	}

	public RequestHolder getRequest(tc = getTestCase().getName(), ts = getTestSuite().getName(), String step, exactMatch=false) {
		setTestCase(ts, tc)
		String key = getTestSuite().getName() + "_" + getTestCase().getName() + "_" + getTestStep(step, exactMatch).getName()
		setTestCaseToDefault()
		return executedSteps.get(key).getRequest()
	}

	public ResponseHolder getResponse(tc = getTestCase().getName(), ts = getTestSuite().getName(), String step, exactMatch=false) {
		setTestCase(ts, tc)
		String key = getTestSuite().getName() + "_" + getTestCase().getName() + "_" + getTestStep(step, exactMatch).getName()
		setTestCaseToDefault()
		return executedSteps.get(key).getResponse()
	}

	public RequestHolder getLastRequest() {
		return executedSteps.get(lastStepRan).getRequest()
	}

	public ResponseHolder getLastResponse() {
		return executedSteps.get(lastStepRan).getResponse()
	}

	public String getLastStepRan() {
		return this.lastStepRan
	}

	public String getLastStepStatus() {
		return lastStepStatus
	}

	public void verifyResponseTemplate(def idNode, def record) {
		def expectedMap = [:]

		record.each {
			if (getLastRequest().getNodeCount("ns:ResponseTemplate/$it.key") > 0) {
				expectedMap.put(idNode + "[.='" + record.get(idNode) + "']/../$it.key", Evaluate.EQUAL)
			} else {
				expectedMap.put(idNode + "[.='" + record.get(idNode) + "']/../$it.key", Evaluate.IS_NULL)
			}
		}

		expectedMap.each {
			ResponseHolder rh = getLastResponse()
			rh.assertAll(it.key, rh.getNodeValue(it.key), it.value)
		}
	}

	public void setWSSecurityConfig(String outgoing = "outgoing", String incoming = "incoming") {
		getTestSteps().each {
			if (it.value instanceof WsdlTestRequestStep) {
				WsdlTestRequest treq = it.value.getTestRequest()
				treq.setEndpoint("\${#Project#WsSecurityEndpoint}")
				treq.setOutgoingWss(outgoing)
				treq.setIncomingWss(incoming)
				WsaConfig wsaConfig = treq.getWsaConfig()
				wsaConfig.setWsaEnabled(true)
				wsaConfig.setVersion("200408")
				wsaConfig.setAction("")
				wsaConfig.setGenerateMessageId(true)
			}
			log.info "COMPLETE"
		}
	}

	public def setTestCaseProperty(k,v) {
		originatingTestCase.setPropertyValue(k,v)
	}

	public def setTestSuiteProperty(k,v) {
		originatingTestCase.testSuite.setPropertyValue(k,v)
	}

	public def setProjectProperty(k,v) {
		getTestSuite().project.setPropertyValue(k,v)
	}

	public def getTestCaseProperty(k) {
		Holder h = new Holder()
		h.setContext(getContext())
		return h.getTestCaseProperty(k)
	}

	public def getTestSuiteProperty(k) {
		Holder h = new Holder()
		h.setContext(getContext())
		return h.getTestSuiteProperty(k)
	}

	public def getProjectProperty(k) {
		Holder h = new Holder()
		h.setContext(getContext())
		return h.getProjectProperty(k)
	}
}