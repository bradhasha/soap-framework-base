public class Dbv {

	public enum Type {
		FILE_TYPE
	}

	private TestRunner tr = null
	private String baseDir = null

	private def jsonMaster = [
		"tpfdds.:.*planid.*seriesid":"1"
	]

	private def dexMaster = [
		"<PLAN":"1"
	]

	private def bulkMaster = [
		"<PLAN>.*":"1"
	]

	public Dbv(TestRunner tr) {
		this.tr = tr
		baseDir = tr.getContext().expand(tr.getTestSuite().project.resourceRoot)
	}

	private void scpFile(String fileName) {
		def scpCommand = "$baseDir/data/scp.sh $fileName $baseDir/data/tmp $baseDir/data/tools.pem.cer".execute()
		scpCommand.waitForOrKill(30000)
	}

	public void verify(String fileName, HashMap<String,String> toVerify, Type type) {
		scpFile(fileName)

		File output = new File("$baseDir/data/tmp/output.txt")
		output.setBytes(new byte[0])
		toVerify.each {
			def sout = new StringBuilder(), serr = new StringBuilder()
			def p1 = [
				"grep",
				"-o",
				it.key,
				"$baseDir/data/tmp/$fileName"
			]
			def p2 = "wc -l"
			def command = p1.execute() | p2.execute()
			command.consumeProcessOutput(sout, serr)
			command.waitForOrKill(30000)

			def master = [:]
			if (type.equals(Type.DEX)) {
				master = dexMaster
			} else if (type.equals(Type.BULK)) {
				master = bulkMaster
			} else if (type.equals(Type.JSON)) {
				master = jsonMaster
			}

			boolean status = sout.toString().trim().equals((it.value.equals("Y") ? master.get(it.key) : "0"))
			tr.getLog().info "`" + it.key + "`" + " has a status of $status" + ", found " + sout.toString().trim() + " match(es), expected " + (it.value.equals("Y") ? master.get(it.key) : "0" )+ " matches(es)"
			assert(status)

			if (it.value.equals("Y")) {
				output << "`" + it.key + "`" + " has " + master.get(it.key) + " match(es)\n"
			} else {
				output << "`" + it.key + "`" + " has no match(es)\n"
			}
		}
		tr.getLog().info "\n$output"
		tr.getLog().info "COMPLETED VERIFICATION OF " + type.name() + " FILE $fileName"
	}
}