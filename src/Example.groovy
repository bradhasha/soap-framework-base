import java.util.HashMap
import java.util.List

class Example {
	
	public HashMap<String,Holder> createSingle(String main = "default", String item) {
		return getTestRunner().runStep("TestCase", "TestSuite", "Request", [
			"main":"newmainvalue",
			"item":"itemvalue"
		])
	}

	public HashMap<String,Holder> createMultiple(String main = "main", List<String> items) {
		HashMap<String, Holder> res = new HashMap()
		// If less than 8 use one of our base requests
		if (items.size() < 8) {
			HashMap<String,String> data = new HashMap<String,String>()
			data.put("main", "newmainvalue")

			items.eachWithIndex { next, index ->
				data.put("item[" + (index + 1) + "]", next)
			}
			res = getTestRunner().runStep("TestCase", "TestSuite", "Request-" + items.size(), data)
			getJobStatus(getTestRunner().getLastResponse().getNodeValue("JobId"))
			return res
			// Otherwise use the single create service
		} else {
			cins.each {
				res = createCin(pid, it, ccc1, ccc2, ccc3)
			}
		}
		return res
	}

}
