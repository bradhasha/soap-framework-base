import com.eviware.soapui.support.GroovyUtils
import com.eviware.soapui.support.XmlHolder

class ResponseHolder extends Assertion {
	
	public ResponseHolder() {
		
	}
	
	@Override
	public def initializeHolder() {
		// Initialize response holder
		try {
			setHolder(new XmlHolder(getMessageExchange().responseContentAsXml))
		} catch (Exception e) {
			setHolder(new GroovyUtils(getContext()).getXmlHolder(getMessageExchange()))
		}
	}
	
	@Override
	public def setMessageExchange(messageExchange) {
		super.setMessageExchange(messageExchange)
		initializeHolder()
	}
	
	public def assertMessageBodyContains(expected) {
		assert getHolder().getXml().contains(expected), "Message body does not contain expected value $expected"
	}

	public def assertMessageBodyDoesNotContains(expected) {
		assert !getHolder().getXml().contains(expected), "Message body does contain unexpected value $expected"
	}
}
