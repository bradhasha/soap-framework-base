class Holder {

	// Main components
	private def holder
	private def messageExchange
	private def context
	private def log

	public def setHolder(holder) {
		this.holder = holder
	}

	public def setMessageExchange(messageExchange) {
		this.messageExchange = messageExchange
	}

	public def setContext(context) {
		this.context = context
	}

	public def setLog(log) {
		this.log = log
	}
	
	public def getHolder() {
		return this.holder
	}

	public def getMessageExchange() {
		return this.messageExchange
	}

	public def getContext() {
		return this.context
	}
	
	public def getLog() {
		return this.log
	}
	
	public def getTestCaseProperty(def prop) {
		return getContext().expand('${#TestCase#' + "$prop" + '}')
	}

	public def getTestSuiteProperty(def prop) {
		return getContext().expand('${#TestSuite#' + "$prop" + '}')
	}
	
	public def getProjectProperty(def prop) {
		return getContext().expand('${#Project#' + "$prop" + '}')
	}
	
	public def initializeHolder() {
		
	}
}
